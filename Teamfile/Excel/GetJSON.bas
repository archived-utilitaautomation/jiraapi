Attribute VB_Name = "GetJSON"
Option Explicit

Sub StoryInfo()

    Dim objRequest As Object
    Dim strUrl As String
    Dim blnAsync As Boolean
    Dim strResponse As String
    Dim body As String
    Dim ticket As String
    
    'Get the ticket value out of cell B1
    ticket = Sheets("Sheet1").Range("B1").Value
    
    'This is to clear the existing table data, otherwise it will look odd if teh new table is smaller than the existing one
    Sheets("Sheet1").Range("A4").Select
    If Not Selection.ListObject.DataBodyRange Is Nothing Then
        Selection.ListObject.DataBodyRange.Rows.Delete
    End If
    'This just means wait until this has finished before doing anything else
    DoEvents
    
    'Set an object ready for the API call
    Set objRequest = CreateObject("MSXML2.XMLHTTP")
    strUrl = "https://utilita.atlassian.net/rest/api/latest/issue/" & ticket
    blnAsync = True
    
    'With objRequest just means do everything in the block to that object
    'So .Open really means objRequest.Open
    With objRequest
        .Open "GET", strUrl, blnAsync
        .SetRequestHeader "Authorization", "Basic Y2hyaXNsaXBzY29tYmVAdXRpbGl0YS5jby51azp0VUJjbGRQOGVlazRBVFdjZlZhcTcxRkY="
        .SetRequestHeader "Content-Type", "application/json"
        .Send body
        'spin wheels whilst waiting for response
        While objRequest.ReadyState <> 4
            DoEvents
        Wend
        strResponse = .ResponseText
    End With
    
    
    Dim Parsed As Object
    Dim totalCount As Integer
    Dim subCount As Integer
    Dim linkCount As Integer
    
    'JsonConverter is the module that has been inported to parse the JSON
    'JsonConverter.ParseJson converts the JSON into a nice handy collection
    Set Parsed = JsonConverter.ParseJson(strResponse)
    
    'I get a count of how many sub tasks there are and a count of how many linked items there are so that I can set the limit
    'of the variant object I'm using to store the data I want to show
    subCount = Parsed("fields")("subtasks").count
    linkCount = Parsed("fields")("issuelinks").count
    totalCount = subCount + linkCount
    
    'Dim my variant object
    Dim Values As Variant
    'This redim just sets the number of rows I will need it to have and there are up to 6 fields in each row
    ReDim Values(totalCount, 6)
    
    'Setup a temp dictionary to hold the values of each row to be processed
    Dim Value As Dictionary
    
    'Setup my counter
    Dim i As Long
    
    'For the story I am looking for I can add the data straight into the first row
    Values(0, 0) = Parsed("fields")("issuetype")("name")
    Values(0, 1) = Parsed("key")
    Values(0, 2) = Parsed("fields")("summary")
    Values(0, 3) = Parsed("fields")("priority")("name")
    Values(0, 4) = Parsed("fields")("status")("name")
    Values(0, 5) = Parsed("fields")("timetracking")("timeSpent")
    
    'i starts at 1 because I've already added a row
    i = 1
    
    'Loop through the subtasks and add the data
    For Each Value In Parsed("fields")("subtasks")
        Values(i, 0) = Value("fields")("issuetype")("name")
        Values(i, 1) = Value("key")
        Values(i, 2) = Value("fields")("summary")
        Values(i, 3) = Value("fields")("priority")("name")
        Values(i, 4) = Value("fields")("status")("name")
        i = i + 1
    Next Value
    
    'Only need to do this bit if there are linked issues
    If linkCount > 0 Then
        'Loop through teh linked issues
        For Each Value In Parsed("fields")("issuelinks")
            'Linked issues can wither be inward or outward (I don't know why or what the difference is)
            'If there is data in inwardIssues then we use that, otherwise use the outwardIssues data
            If Value.Exists("inwardIssue") Then
                Values(i, 0) = "Linked " & Value("inwardIssue")("fields")("issuetype")("name")
                Values(i, 1) = Value("inwardIssue")("key")
                Values(i, 2) = Value("inwardIssue")("fields")("summary")
                Values(i, 3) = Value("inwardIssue")("fields")("priority")("name")
                Values(i, 4) = Value("inwardIssue")("fields")("status")("name")
            Else
                Values(i, 0) = "Linked " & Value("outwardIssue")("fields")("issuetype")("name")
                Values(i, 1) = Value("outwardIssue")("key")
                Values(i, 2) = Value("outwardIssue")("fields")("summary")
                Values(i, 3) = Value("outwardIssue")("fields")("priority")("name")
                Values(i, 4) = Value("outwardIssue")("fields")("status")("name")
            End If
            i = i + 1
        Next Value
    End If
    
    'Then dump the entire new variant object into the spreadsheet starting at row A4
    Sheets("Sheet1").Range("A4", Cells(totalCount + 4, 6)) = Values
    
End Sub

'This sub is the same except it adds to the cells as it runs, looks sloppy compared to dumping it all in one go
'But with this one you would see it progressing if there were a lot of subtasks and linked issues to add
Sub StoryInfo2()

    Dim objRequest As Object
    Dim strUrl As String
    Dim blnAsync As Boolean
    Dim strResponse As String
    Dim body As String
    Dim ticket As String
    
    ticket = Sheets("Sheet1").Range("B1").Value
    Sheets("Sheet1").Range("A4").Select
    If Not Selection.ListObject.DataBodyRange Is Nothing Then
        Selection.ListObject.DataBodyRange.Rows.Delete
    End If
    DoEvents
        
    Set objRequest = CreateObject("MSXML2.XMLHTTP")
    strUrl = "https://utilita.atlassian.net/rest/api/latest/issue/" & ticket
    blnAsync = True
    
    With objRequest
        .Open "GET", strUrl, blnAsync
        .SetRequestHeader "Authorization", "Basic Y2hyaXNsaXBzY29tYmVAdXRpbGl0YS5jby51azp0VUJjbGRQOGVlazRBVFdjZlZhcTcxRkY="
        .SetRequestHeader "Content-Type", "application/json"
        .Send body
        'spin wheels whilst waiting for response
        While objRequest.ReadyState <> 4
            DoEvents
        Wend
        strResponse = .ResponseText
    End With
    
    
    Dim Parsed As Object
    Set Parsed = JsonConverter.ParseJson(strResponse)
    
    Dim linkCount As Integer
    linkCount = Parsed("fields")("issuelinks").count

    Dim Value As Dictionary
    Dim i As Long
    i = 4
    
    Sheets("Sheet1").Range("A" & i).Value = Parsed("fields")("issuetype")("name")
    Sheets("Sheet1").Range("B" & i).Value = Parsed("key")
    Sheets("Sheet1").Range("C" & i).Value = Parsed("fields")("summary")
    Sheets("Sheet1").Range("D" & i).Value = Parsed("fields")("priority")("name")
    Sheets("Sheet1").Range("E" & i).Value = Parsed("fields")("status")("name")
    Sheets("Sheet1").Range("F" & i).Value = Parsed("fields")("timetracking")("timeSpent")
    i = i + 1
    
    For Each Value In Parsed("fields")("subtasks")
        Sheets("Sheet1").Range("A" & i).Value = Value("fields")("issuetype")("name")
        Sheets("Sheet1").Range("B" & i).Value = Value("key")
        Sheets("Sheet1").Range("C" & i).Value = Value("fields")("summary")
        Sheets("Sheet1").Range("D" & i).Value = Value("fields")("priority")("name")
        Sheets("Sheet1").Range("E" & i).Value = Value("fields")("status")("name")
        i = i + 1
    Next Value
    
    If linkCount > 0 Then
        'Sort Linked Issues
        For Each Value In Parsed("fields")("issuelinks")
            If Value.Exists("inwardIssue") Then
                Sheets("Sheet1").Range("A" & i).Value = "Linked " & Value("inwardIssue")("fields")("issuetype")("name")
                Sheets("Sheet1").Range("B" & i).Value = Value("inwardIssue")("key")
                Sheets("Sheet1").Range("C" & i).Value = Value("inwardIssue")("fields")("summary")
                Sheets("Sheet1").Range("D" & i).Value = Value("inwardIssue")("fields")("priority")("name")
                Sheets("Sheet1").Range("E" & i).Value = Value("inwardIssue")("fields")("status")("name")
            Else
                Sheets("Sheet1").Range("A" & i).Value = "Linked " & Value("outwardIssue")("fields")("issuetype")("name")
                Sheets("Sheet1").Range("B" & i).Value = Value("outwardIssue")("key")
                Sheets("Sheet1").Range("C" & i).Value = Value("outwardIssue")("fields")("summary")
                Sheets("Sheet1").Range("D" & i).Value = Value("outwardIssue")("fields")("priority")("name")
                Sheets("Sheet1").Range("E" & i).Value = Value("outwardIssue")("fields")("status")("name")
            End If
            i = i + 1
        Next Value
    End If
    
End Sub


